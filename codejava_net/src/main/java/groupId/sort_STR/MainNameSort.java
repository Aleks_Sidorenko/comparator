package groupId.sort_STR;

import groupId.Employee;

import java.util.Arrays;

/**
 * Created on 25.05.2021 14:54.
 *
 * @author Aleks Sidorenko (e-mail: alek.sidorenko@gmail.com).
 * @version Id$.
 * @since 0.1.
 */
public class MainNameSort {

    public static void main(String[] args) {
        Employee[] newEmployees = new Employee[4];

        newEmployees[0] = new Employee("Tom", 45, 80000);
        newEmployees[1] = new Employee("Sam", 56, 75000);
        newEmployees[2] = new Employee("Alex", 30, 120000);
        newEmployees[3] = new Employee("Peter", 25, 60000);

        //System.out.println("Before sorting: " + Arrays.toString(newEmployees));
        System.out.println("Before sorting: ");
        for (Employee e: newEmployees) {
            System.out.println("Name=" + e.getName() + ", age= " + e.getAge() + ", salary=" + e.getSalary());
        }
        Arrays.sort(newEmployees, new EmployeeNameComparator());

        //System.out.println("After sorting: " + Arrays.toString(newEmployees));
        System.out.println("After sorting: ");
        for (Employee e: newEmployees) {
            System.out.println("Name=" + e.getName() + ", age= " + e.getAge() + ", salary=" + e.getSalary());
        }
    }
}
